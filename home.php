<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>MyNews</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Font Awesome JS -->
    <script src="js/solid.js"></script>
    <script src="js/fontawesome.js"></script>

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div id="sidebar conteneur" class="sticky-top">
                <div class="sidebar-header">
                    <h3><img src="img/Logo2.png" alt="MyNews" class="img-fluid center"></h3>
                    <strong><img src="img/logo-image-only.png" alt="MN" class="img-fluid center"></strong>
                </div>

                <ul class="list-unstyled components">
                    <li class="active">
                        <a href="#InternationalSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle center">
                            <i class="fas fa-globe"></i>
                            International
                        </a>
                        <ul class="collapse list-unstyled" id="InternationalSubmenu">
                            <li>
                                <a href="#uneInter"> <i class="fas fa-newspaper"></i> À la une</a>
                            </li>
                            <li>
                                <a href="#uneEuro"><i class="fas fa-euro-sign"></i> Europe</a>
                            </li>
                            <li>
                                <a href="#SocieteInternational"><i class="fas fa-users"></i> Société</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#franceSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-home"></i>
                            France
                        </a>
                        <ul class="collapse list-unstyled" id="franceSubmenu">
                            <li>
                                <a href="#unePolitique"><i class="fas fa-handshake"></i> Politique</a>
                            </li>
                            <li>
                                <a href="#uneEconomie"><i class="fas fa-briefcase"></i> Economie</a>
                            </li>
                            <li>
                                <a href="#uneEducation"><i class="fas fa-graduation-cap"></i> Education</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#cultureSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-book-open"></i>
                            Culture
                        </a>
                        <ul class="collapse list-unstyled" id="cultureSubmenu">
                            <li>
                                <a href="#medium"><i class="fas fa-film"></i> Médias</a>
                            </li>
                            <li>
                                <a href="#musqiue"><i class="fas fa-music"></i> Musique</a>
                            </li>
                            <li>
                                <a href="#livre"><i class="fas fa-book"></i> Livre</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#sportSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-futbol"></i>
                            Sport
                        </a>
                        <ul class="collapse list-unstyled" id="sportSubmenu">
                            <li>
                                <a href="#toutlesport"> <i class="fas fa-news-paper"></i>À la une </a>
                            </li>
                            <li>
                                <a href="#foot"> <i class="fas fa-futbol"></i> Football</a>
                            </li>
                            <li>
                                <a href="#rugby"><i class="fas fa-football-ball"></i> Rugby</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#sciSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-cogs"></i>
                            Sciences
                        </a>
                        <ul class="collapse list-unstyled" id="sciSubmenu">
                            <li>
                                <a href="#espace"><i class="fas fa-user-astronaut"></i> Espace</a>
                            </li>
                            <li>
                                <a href="#informatique"><i class="fas fa-desktop"></i> Informatique</a>
                            </li>
                            <li>
                                <a href="#sante"> <i class="fas fa-heartbeat"></i> Santé</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#planeteSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-leaf"></i>
                            Planète
                        </a>
                        <ul class="collapse list-unstyled" id="planeteSubmenu">
                            <li>
                                <a href="#climat"> <i class="fas fa-cloud"></i> Climat</a>
                            </li>
                            <li>
                                <a href="#environnement"> <i class="fas fa-recycle"></i> Environnement</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="https://gitlab.com/JulianMot/projet-flux-rss/issues">
                            <i class="fas fa-question"></i>
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="https://gitlab.com/JulianMot/projet-flux-rss">
                            <i class="fas fa-paper-plane"></i>
                            Contact
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="btn btn-info-sidebar">
                        <i class="fas fa-align-left"></i>
                        <span>Menu</span>
                    </button>
                </div>
            </nav>
            <div>

                <!----------------------------------------- FONCTION FLUX RSS  --------------------------------------->
                <?php
                function flux_rss($feed1, $feed2, $feed3)
                {
                    // on charge 5 actualités à chaque fois
                    for ($i = 0; $i < 5; $i++) {
                        $flux_alea = rand(1, 3); // random de 1 à 3 afin de détemriner quel source on prend entre BFM, Le Figaro ou le Monde
                        switch ($flux_alea) {
                            case 1:
                                $url = $feed1;
                                break;
                            case 2:
                                $url = $feed2;
                                break;
                            case 3:
                                $url = $feed3;
                                break;
                            default:
                                $url = $feed1;
                        }
                        $rss = simplexml_load_file($url);
                        $datetime = date_create($rss->channel->item[$i]->pubDate); // on récupère l'heure du flux
                        $date = date_format($datetime, 'd M , H\hi'); // on récupère la date du flux
                        echo '<li><a href="' . $rss->channel->item[$i]->link . '">' . $rss->channel->item[$i]->title . '</a> (' . $date . ')</li>'; // lien + titre + date
                        echo '<p>' . $rss->channel->item[$i]->description . '</p>'; // description
                        // Récupère une partie de l'url pour afficher la source
                        if (substr($rss->channel->item[$i]->link, 8, 8) == "www.lemo")
                            $source = "source : Le Monde &copy";
                        else {
                            if (substr($rss->channel->item[$i]->link, 8, 8) == "www.lefi")
                                $source = "source : Le Figaro &copy";
                            else {
                                $source = "source : BFM &copy";
                            }
                        }
                        echo "<p> $source </p>"; // copyright
                        echo '</ul>';
                    }
                }
                echo '</ul>';
                ?>
            </div>
            <!-------------------------------------- FLUX RSS INTERNATIONAL / À la une---------------------------------->
            <h2> Actualités Internationales </h2>
            <div class="news" id="uneInter">
                <h3> À la une </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/international/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_international.xml";
                $feed3 = "https://www.lemonde.fr/international/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>

            <!-------------------------------------- FIN FLUX RSS INTERNATIONAL / À la une ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS INTERNATIONAL / EUROPE ---------------------------------->
            <div class="news" id="uneEuro">
                <h3> Europe </h3>
                <?php
                $feed1 = "https://www.rtl.fr/sujet/europe.rss";
                $feed2 = "http://plus.lefigaro.fr/tag/europe/rss.xml";
                $feed3 = "https://www.lemonde.fr/europe/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS INTERNATIONAL / EUROPE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS INTERNATIONAL / SOCIETE ---------------------------------->
            <div class="news" id="SocieteInternational">
                <h3> Société </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_actualites-a-la-une.xml";
                $feed3 = "https://www.lemonde.fr/m-actu/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS INTERNATIONAL / SOCIETE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS FRANCE / POLITQUE ---------------------------------->
            <h2> Actualités France </h2>
            <div class="news" id="unePolitique">
                <h3> Politique </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/politique/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_politique.xml";
                $feed3 = "https://www.lemonde.fr/politique/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS FRANCE / POLITIQUE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS FRANCE / ECONOMIE ---------------------------------->
            <div class="news" id="uneEconomie">
                <h3> Economie </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/economie/actualite/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_economie.xml";
                $feed3 = "https://www.lemonde.fr/economie/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS FRANCE / POLITIQUE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS FRANCE / EDUCATION ---------------------------------->
            <div class="news" id="uneEducation">
                <h3> Education </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/culture/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_culture.xml";
                $feed3 = "https://www.lemonde.fr/economie-francaise/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS FRANCE / POLITIQUE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS CULTURE / Medias ---------------------------------->
            <h2> Actualités Culture </h2>
            <div class="news" id="medium">
                <h3> Actualités Média </h3>
                <?php
                $feed1 = "https://vous.bfmtv.com/rss/actu-tele/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_medias.xml";
                $feed3 = "https://www.lemonde.fr/televisions-radio/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS CULTURE / Medias ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS CULTURE / Musique ---------------------------------->
            <div class="news" id="musqiue">
                <h3> Actualités Musique </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/culture/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_musique.xml";
                $feed3 = "https://www.lemonde.fr/musiques/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS CLUTUE / Musique ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS CULTURE / LIVRE ---------------------------------->
            <div class="news" id="livre">
                <h3> Actualités Livres </h3>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/culture/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_livres.xml";
                $feed3 = "https://www.lemonde.fr/livres/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS CLUTUE / LIVRE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS SPORT ---------------------------------->
            <h3> Actualités Sport </h3>
            <div class="news" id="toutlesport">
                <h2> À la une </h2>
                <?php
                $feed1 = "http://podcast.rmc.fr/channel230/RMCInfochannel230.xml";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_sport.xml";
                $feed3 = "https://www.lemonde.fr/sport/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS SPORT ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS SPORT / FOOT ---------------------------------->
            <div class="news" id="foot">
                <h2> Actualités Foot </h2>
                <?php
                $feed1 = "https://rmcsport.bfmtv.com/rss/football/";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_football.xml";
                $feed3 = "https://www.lemonde.fr/football/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS SPORT / FOOT ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS SPORT / RUGBY ---------------------------------->
            <div class="news" id="rugby">
                <h2> Actualités Rugby </h2>
                <?php
                $feed1 = "http://podcast.rmc.fr/channel230/RMCInfochannel230.xml";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_rugby.xml";
                $feed3 = "https://www.lemonde.fr/sport/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS SPORT / RUGBY ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS SCIENCE /TECHNO ---------------------------------->
            <h3> Actualités Science </h3>
            <div class="news" id="espace">
                <h2> Espace </h2>
                <?php
                $feed1 = "https://www.lemonde.fr/espace/rss_full.xml";
                $feed2 = "https://www.cieletespace.fr/rss.xml";
                $feed3 = "https://www.futura-sciences.com/rss/espace/actualites.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS SCIENCE /TECHNO ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS SCIENCE / INFO ---------------------------------->
            <div class="news" id="informatique">
                <h2> Informatique </h2>
                <?php
                $feed1 = "https://www.lemonde.fr/big-browser/rss_full.xml";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_secteur_high-tech.xml";
                $feed3 = "https://www.lefigaro.fr/rss/figaro_secteur_high-tech.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS SCIENCE /INFO ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS SCIENCE / SANTE ---------------------------------->
            <div class="news" id="sante">
                <h2> Santé </h2>
                <?php
                $feed1 = "https://www.lefigaro.fr/rss/figaro_sante.xml";
                $feed2 = "https://www.lemonde.fr/sante/rss_full.xml";
                $feed3 = "https://www.lemonde.fr/sante/rss_full.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS SCIENCE / SANTE ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS PLANETE / CLIMAT ---------------------------------->
            <h3> Actualités Planète </h3>
            <div class="news" id="climat">
                <h2> Climat </h2>
                <?php
                $feed1 = "https://www.lemonde.fr/climat/rss_full.xml";
                $feed2 = "https://www.lefigaro.fr/rss/figaro_sante.xml";
                $feed3 = "https://www.bfmtv.com/rss/planete/";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS PLANETE /CLIMAT ---------------------------------->
            <div class="line"></div>
            <!-------------------------------------- FLUX RSS PLANETE / ENVIRONNEMENT ---------------------------------->
            <div class="news" id="environnement">
                <h2> Environnement </h2>
                <?php
                $feed1 = "https://www.bfmtv.com/rss/planete/";
                $feed2 = "https://www.lemonde.fr/planete/rss_full.xml";
                $feed3 = "http://plus.lefigaro.fr/tag/dechets/rss.xml";
                flux_rss($feed1, $feed2, $feed3);
                ?>
            </div>
            <!-------------------------------------- FIN FLUX RSS PLANETE  / ENVIRONNEMENT ---------------------------------->

        </div>

        <!-- jQuery CDN - Slim version (=without AJAX) -->
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <!-- Popper.JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#sidebarCollapse').on('click', function() {
                    $('#sidebar').toggleClass('active');
                });
            });
        </script>
</body>

</html>